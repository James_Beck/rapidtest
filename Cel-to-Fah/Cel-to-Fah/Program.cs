﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cel_to_Fah
{
    class Program
    {
        static void Main(string[] args)
        {
            string _response;
            int? value;

            do
            {
                Console.WriteLine("What if the temperature scale you are using?");
                _response = Response();
                Console.Clear();
            } while (_response == "Try again");

            do
            {
                Console.WriteLine($"What is the temperature in {_response}");
                value = int.Parse(Console.ReadLine());
                Console.Clear();
            } while (value == null);

            if (_response == "celcius")
            {
                Console.WriteLine($"The temperature {value} in {_response} is also equal to {Fahrenheit((double)value)} in fahrenheit");
            }
            else
            {
                Console.WriteLine($"The temperature {value} in {_response} is also equal to {Celcius((double)value)} in fahrenheit");
            }
        }

        static double Fahrenheit(double celcius)
        {
            return Math.Round((celcius * 9 / 5 + 32), 2);
        }

        static double Celcius(double fahrenheit)
        {
            return Math.Round(((fahrenheit - 32) * 5 / 9), 2);
        }

        static string Response()
        {
            string response = Console.ReadLine().ToLower();

            if (response == "celcius" || response == "fahrenheit")
            {
                return response;
            }

            return "Try again";
        }
    }
}
