﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberAverage
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> values = new List<int>();

            for (int i = 0; i < 10; i++)
            {
                values.Add(int.Parse(Console.ReadLine()));
            }

            Console.WriteLine(values.Average());
        }
    }
}
